import http from 'node:http';
import path from 'node:path';
import {writeFile, mkdir} from 'node:fs/promises';
import {env} from 'node:process';
import test from 'ava';
import {stub, match, restore} from 'sinon';
import {temporaryDirectory} from 'tempy';
import clearModule from 'clear-module';
import {WritableStreamBuffer} from 'stream-buffers';
import express from 'express';
import listen from 'test-listen';

test.beforeEach(async t => {
	// Clear npm cache to refresh the module state
	clearModule('..');
	t.context.m = await import('../plugin.mjs');

	// Stub the logger
	t.context.log = stub();
	t.context.stdout = new WritableStreamBuffer();
	t.context.stderr = new WritableStreamBuffer();
	t.context.logger = {log: t.context.log, success: t.context.log, warn: t.context.log};

	// Create temporary working directory
	t.context.cwd = temporaryDirectory();

	// Create the mock Docker socket
	const app = express();
	t.context.middleware = stub().callsFake((request, response) => response.json({}));
	app.get('/info', t.context.middleware);
	app.post('/images/create', t.context.middleware);
	app.post('/images/*/tag', t.context.middleware);
	app.post('/images/*/push', t.context.middleware);
	app.post('/auth', t.context.middleware);

	t.context.server = http.createServer(app);
	t.context.url = await listen(t.context.server);

	// Create the `semantic-release` context
	t.context.ctx = {
		cwd: t.context.cwd,
		stdout: t.context.stdout,
		stderr: t.context.stderr,
		logger: t.context.logger,
		options: {},
		env: {
			CI_REGISTRY_IMAGE: 'gitlab.arm.com:5050/semantic-release/dockerode-tag',
			CI_COMMIT_SHA: 'abc',
			DOCKER_HOST: t.context.url,
		},
	};

	// Create the `semantic-release` config
	t.context.cfg = {
		images: [
			// eslint-disable-next-line no-template-curly-in-string
			'${CI_REGISTRY_IMAGE}/test:${CI_COMMIT_SHA}',
			{
				// eslint-disable-next-line no-template-curly-in-string
				image: '${CI_REGISTRY_IMAGE}/test:${CI_COMMIT_SHA}',
				// eslint-disable-next-line no-template-curly-in-string
				tag: '${nextRelease.version}-${tag.slice(0, 8)}',
			},
		],
	};
});

test.afterEach(t => {
	t.context.server.close();
	restore();
});

const cfg = test.macro({
	async exec(t, before) {
		await before(t);

		const pattern = t.title.replace(/^.+ when /, '');
		const regex = new RegExp(`${pattern[0].toUpperCase()}${pattern.slice(1)}`);

		const error = await t.throwsAsync(t.context.m.verifyConditions(t.context.cfg, t.context.ctx));

		t.is(error.name, 'SemanticReleaseError', `${error}`);
		t.is(error.code, 'EDOCKERODECFG', `${error}`);
		t.regex(error.message, regex, `${error}`);
	},
	title: title => `Throws error when ${title[0].toLowerCase()}${title.slice(1)}`,
});

const success = test.macro({
	async exec(t, before, endpoints = [
		'/info',
		'/images/create',
		'/images/create',
		'/info',
		'/images/create',
		'/images/create',
		'/images/*/tag',
		'/images/*/tag',
		'/images/*/push',
		'/images/*/push',
	]) {
		if (before) {
			await before(t);
		}

		await t.notThrowsAsync(t.context.m.verifyConditions(t.context.cfg, t.context.ctx));
		t.context.ctx.nextRelease = {version: 'test'};
		await t.notThrowsAsync(t.context.m.publish(t.context.cfg, t.context.ctx));

		const routes = t.context.middleware.getCalls().map(({args: [request, _]}) => request.route.path);

		t.deepEqual(routes, endpoints);
	},
	title: title => title ? `Tag images with ${title}` : 'Tag images',
});

const endpoint = test.macro({
	async exec(t, pattern, before) {
		if (typeof pattern === 'string') {
			pattern = new RegExp(pattern);
		}

		if (before) {
			await before(t);
		}

		const endpoint = t.title.match(/`(.+)`/)[1];

		t.context.middleware
			.withArgs(match.has('route', match.has('path', endpoint)))
			.callsFake((request, response) => {
				response.status(500);
				response.json({message: 'forced error'});
			});

		try {
			await t.context.m.verifyConditions(t.context.cfg, t.context.ctx);
			t.context.ctx.nextRelease = {version: 'test'};
			await t.context.m.publish(t.context.cfg, t.context.ctx);
		} catch (error) {
			t.is(error.name, 'SemanticReleaseError', `${error}`);
			t.is(error.code, 'EDOCKERODE', `${error}`);
			t.regex(error.message, pattern, `${error}`);
		}
	},
	title: endpoint => `Throws error if Docker \`${endpoint}\` end point fails`,
});

// Configuration tests

test('Failed to parse Docker host as URL', cfg, async t => {
	t.context.ctx.env.DOCKER_HOST = '://';
});

test('`images` are undefined', cfg, async t => {
	delete t.context.cfg.images;
});

test('`images` are not iterable', cfg, async t => {
	t.context.cfg.images = 1;
});

test('`images` are not mappable', cfg, async t => {
	t.context.cfg.images = 'a';
});

test('`image` must be a string', cfg, async t => {
	t.context.cfg.images = [{tag: 'something'}];
});

test('`image` could not be templated', cfg, async t => {
	// eslint-disable-next-line no-template-curly-in-string
	t.context.cfg.images = [{image: '${THIS_DOES_NOT_EXIST}', tag: 'something'}];
});

test('`tag` must be a string', cfg, async t => {
	t.context.cfg.images = [{image: 'something'}];
});

test('`tag` could not be templated', cfg, async t => {
	// eslint-disable-next-line no-template-curly-in-string
	t.context.cfg.images = [{image: 'something', tag: '${THIS_DOES_NOT_EXIST}'}];
});

test('Failed to read Docker TLS certificates from `.+`', cfg, async t => {
	t.context.cfg.certPath = temporaryDirectory();
});

test('Failed to parse Docker timeout', cfg, async t => {
	t.context.cfg.timeout = Symbol.iterator;
});

test('Failed to parse Docker timeout as integer', cfg, async t => {
	t.context.cfg.timeout = 'whatever';
});

test('Unsupported protocol `.+`', cfg, async t => {
	t.context.cfg.host = 'abc://whatever';
});

test('Must provide `password` as well as `username`', cfg, async t => {
	t.context.cfg.username = 'Simon Segars';
});

test('Must provide `username` as well as `password`', cfg, async t => {
	t.context.cfg.password = 'Is awesome!';
});

// Endpoint tests

test('/info', endpoint, '`dockerode` instance could not report `info`');

test('/images/create', endpoint, /Failed to pull `.+`/);

test('/auth', endpoint, 'Failed to get authentication token', async t => {
	t.context.cfg.username = 'Simon Segars';
	t.context.cfg.password = 'Is awesome!';
});

test('/images/*/tag', endpoint, /Failed to tag `.+`/);

test('/images/*/push', endpoint, /Failed to push `.+`/);

// Successful tests

test(success);

test('without protocol', success, async t => {
	t.context.ctx.env.DOCKER_HOST = t.context.ctx.env.DOCKER_HOST.replace('http://', '');
});

test('certificates', success, async t => {
	t.context.cfg.certPath = temporaryDirectory();
	await writeFile(path.join(t.context.cfg.certPath, 'ca.pem'), '-BEGIN CERTIFICATE-\n-END CERTIFICATE-');
	await writeFile(path.join(t.context.cfg.certPath, 'cert.pem'), '');
	await writeFile(path.join(t.context.cfg.certPath, 'key.pem'), '');
});

test('DOCKER_TLS_CERTDIR', success, async t => {
	const temporary = temporaryDirectory();
	t.context.ctx.env.DOCKER_TLS_CERTDIR = temporary;
	await mkdir(path.join(temporary, 'client'));
	await writeFile(path.join(temporary, 'client', 'ca.pem'), '-BEGIN CERTIFICATE-\n-END CERTIFICATE-');
	await writeFile(path.join(temporary, 'client', 'cert.pem'), '');
	await writeFile(path.join(temporary, 'client', 'key.pem'), '');
});

test('timeout', success, async t => {
	t.context.cfg.timeout = 1000;
});

test('TLS verification off', success, async t => {
	t.context.cfg.tlsVerify = false;
});

test('`tcp:` protocol', success, async t => {
	t.context.cfg.host = t.context.url.replace('http:', 'tcp:');
});

test('auth', success, async t => {
	t.context.cfg.username = 'Simon Segars';
	t.context.cfg.password = 'Is awesome!';
}, [
	'/info',
	'/auth',
	'/images/create',
	'/images/create',
	'/info',
	'/auth',
	'/images/create',
	'/images/create',
	'/images/*/tag',
	'/images/*/tag',
	'/images/*/push',
	'/images/*/push',
]);

// Custom tests

test('Throws error when TLS verification is turned on for HTTP socket', async t => {
	t.context.cfg.tlsVerify = true;

	const error = await t.throwsAsync(t.context.m.verifyConditions(t.context.cfg, t.context.ctx));

	t.is(error.name, 'SemanticReleaseError');
	t.is(error.code, 'EDOCKERODE');
	t.is(error.details.code, 'EPROTO');
	t.is(error.message, '`dockerode` instance could not report `info`');
});

// Real tests

const testIf = (env.CI_REGISTRY_IMAGE && env.CI_COMMIT_SHA && env.DOCKER_HOST) ? test : test.skip;
testIf('Docker daemon', success, async t => {
	t.timeout(5 * 60 * 1000);
	const keys = [
		'CI_REGISTRY_IMAGE',
		'CI_COMMIT_SHA',
		'DOCKER_HOST',
		'DOCKER_TLS_VERIFY',
		'DOCKER_CERT_PATH',
		'DOCKER_TLS_CERTDIR',
		'DOCKER_CLIENT_TIMEOUT',
		'DOCKERODE_USERNAME',
		'DOCKERODE_PASSWORD',
		'DOCKERODE_EMAIL',
		'DOCKERODE_SERVER',
	];
	for (const key of keys) {
		if (typeof env[key] === 'string') {
			t.context.ctx.env[key] = env[key];
		}
	}
}, []);
