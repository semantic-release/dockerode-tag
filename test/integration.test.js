// eslint-disable-next-line unicorn/prefer-module
const test = require('ava');

test('Can load the CommonJS module', async t => {
	// eslint-disable-next-line unicorn/prefer-module
	const {verifyConditions, publish} = require('../plugin.js');
	const cfg = {images: []};
	const {temporaryFile} = await import('tempy');
	const logger = {log() {}, warn() {}};
	const ctx = {env: {DOCKER_HOST: `tcp://${temporaryFile()}`}, logger};
	await t.notThrowsAsync(verifyConditions(cfg, ctx));
	ctx.nextRelease = {version: 'test'};
	await t.notThrowsAsync(publish(cfg, ctx));
});
