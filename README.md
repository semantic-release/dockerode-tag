# `dockerode-tag`

> A `semantic-release` plugin to pull, tag and push open container images using
> [dockerode][dockerrode]

## Getting Started

```sh
npm config set always-auth true
npm config set @semantic-release:registry https://gitlab.arm.com/api/v4/packages/npm/
npm config set -- '//gitlab.arm.com/api/v4/packages/npm/:_authToken' '${GITLAB_TOKEN}'
export GITLAB_TOKEN="<personal-access-token>"
npm install --save-dev @semantic-release/dockerode-tag
```

Add the following to `.releaserc.yaml`:

```yaml
plugins:
  - path: "@semantic-release"
    images:
      - "${CI_REGISTRY_IMAGE}/node:${CI_COMMIT_SHA}"
```

## Configuration

## `images`

By default the plugin will tag each provided image with the next release version
as a tag.

These are equivalent configurations:

```yaml
plugins:
  - path: "@semantic-release"
    images:
      - "${CI_REGISTRY_IMAGE}/node:${CI_COMMIT_SHA}"
```

```yaml
plugins:
  - path: "@semantic-release"
    images:
      - image: "${CI_REGISTRY_IMAGE}/node:${CI_COMMIT_SHA}"
        tag: "${nextRelease.version}"
```

The previous image tag is available for [templating][template]:

```yaml
plugins:
  - path: "@semantic-release"
    images:
      - image: "${CI_REGISTRY_IMAGE}/node:14-${CI_COMMIT_SHA}"
        tag: "${tag.split('-')[0]}-${nextRelease.version}"
```

## Authentication

When pulling/pushing the containers requires authentication it can be provided
via configuration arguments:

```yaml
plugins:
  - path: "@semantic-release"
    images:
      - "${CI_REGISTRY_IMAGE}/node:${CI_COMMIT_SHA}"
    username: "someone"
    password: "????" # Not recommended, use `DOCKERODE_PASSWORD`
    email: "someone@somewhere.com"
    server: "https://the.container:5050/endpoint/v2"
```

The other option is to use the relevant `DOCKERODE` environment variables:

- `DOCKERODE_USERNAME`: username usually passed to `docker login`
- `DOCKERODE_PASSWORD`: password usually passed to `docker login`
- `DOCKERODE_EMAIL`: not required, will default to an empty string (`''`)
- `DOCKERODE_SERVER`: often required, will default to the `DOCKER_HOST`
  environment variable

## Docker

The plugin respects the following Docker environment variables:

- `DOCKER_HOST`: the Docker socket host, commonly `tcp:///var/run/docker.sock`
- `DOCKER_CERT_PATH`: the location of `ca.pem`, `cert.pem` and `key.pem`
- `DOCKER_TLS_CERTDIR`: the same as `DOCKER_CERT_PATH` just that everything is
  under a nested `client` folder
- `DOCKER_TLS_VERIFY`: ensure we use TLS to verify the socket, useful when
  working with local socket
- `DOCKER_CLIENT_TIMEOUT`: timeout for the communication over the socket

# Recipes

## GitLab CI

GitLab CI [services][services] can provide a running Docker daemon.

GitLab Container Registry authentication for the plugin can be provided via
environment variables.

Add the following to the `semantic-release` job:

```yaml
services:
  - docker:20.10.17-dind
variables:
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKERODE_USERNAME: "${CI_REGISTRY_USER}"
  DOCKERODE_PASSWORD: "${CI_REGISTRY_PASSWORD}"
  DOCKERODE_SERVER: "${CI_REGISTRY}"
```

[dockerode]: https://github.com/apocas/dockerode#readme
[services]: https://docs.gitlab.com/ee/ci/yaml/#services
[template]: https://docs-lodash.com/v4/template/
