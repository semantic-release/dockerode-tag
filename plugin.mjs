import {readFile} from 'node:fs/promises';
import path from 'node:path';
import {URL} from 'node:url';
import SemanticReleaseError from '@semantic-release/error';
import dbg from 'debug';
import Docker from 'dockerode';
import {parseRepositoryTag} from 'dockerode/lib/util.js';
import {template} from 'lodash-es';
import splitca from 'split-ca';

const debug = dbg('semantic-release:dockerode-tag');

function follow(stream, modem, message) {
	return new Promise((resolve, reject) => {
		function onFinished(error, output) {
			/* c8 ignore start */

			if (error) {
				reject(new SemanticReleaseError(message, 'EDOCKERODE', error));
				return;
			}

			for (const {error} of output) {
				if (typeof error === 'string') {
					reject(new SemanticReleaseError(message, 'EDOCKERODE', error));
					return;
				}
			}

			/* c8 ignore stop */

			resolve(output);
		}

		/* c8 ignore start */

		function onProgress(output) {
			debug('%j', output);
		}

		/* c8 ignore stop */

		modem.followProgress(stream, onFinished, onProgress);
	});
}

export async function verifyConditions(pluginConfig, context) {
	const {env, logger} = context;
	let {images} = pluginConfig;

	// Validate the images
	if (images === undefined) {
		throw new SemanticReleaseError('`images` are undefined', 'EDOCKERODECFG', images);
	} else if (typeof images[Symbol.iterator] !== 'function') {
		throw new SemanticReleaseError('`images` are not iterable', 'EDOCKERODECFG', images);
	} else if (typeof images.map !== 'function') {
		throw new SemanticReleaseError('`images` are not mappable', 'EDOCKERODECFG', images);
	}

	if (images.length === 0) {
		logger.warn('Zero images');
		return;
	}

	images = images.map(i => {
		if (typeof i === 'string') {
			// eslint-disable-next-line no-template-curly-in-string
			i = {image: i, tag: pluginConfig.tag ?? '${nextRelease.version}'};
		}

		let {image, tag} = i;

		if (typeof image !== 'string') {
			throw new SemanticReleaseError('`image` must be a string', 'EDOCKERODECFG', image);
		}

		if (typeof tag !== 'string') {
			throw new SemanticReleaseError('`tag` must be a string', 'EDOCKERODECFG', tag);
		}

		try {
			image = template(image)(env);
		} catch (error) {
			throw new SemanticReleaseError('`image` could not be templated', 'EDOCKERODECFG', error);
		}

		try {
			template(tag)({...env, tag: '', nextRelease: {version: ''}});
		} catch (error) {
			throw new SemanticReleaseError('`tag` could not be templated', 'EDOCKERODECFG', error);
		}

		return {image, tag};
	});

	// Create the Dockerode instance
	const docker = await (async () => {
		const {
			host = env.DOCKER_HOST,
			certPath = env.DOCKER_CERT_PATH ?? (typeof env.DOCKER_TLS_CERTDIR === 'string') ? path.join(env.DOCKER_TLS_CERTDIR, 'client') : undefined,
			tlsVerify = env.DOCKER_TLS_VERIFY,
			timeout = env.DOCKER_CLIENT_TIMEOUT,
		} = pluginConfig;

		const options = {};

		if (host !== undefined) {
			const url = (() => {
				try {
					return new URL(host.includes('://') ? host : `tcp://${host}`);
				} catch {
					throw new SemanticReleaseError('Failed to parse Docker host as URL', 'EDOCKERODECFG', host);
				}
			})();

			options.host = url.hostname;
			options.protocol = url.protocol.slice(0, -1);

			if (url.pathname.length > 0) {
				options.socketPath = url.pathname;
			}

			if (url.port.length > 0) {
				options.port = Number.parseInt(url.port, 10);
			}

			if (options.protocol === 'tcp') {
				options.protocol = 'http';
			}

			if (!options.protocol.startsWith('http')) {
				throw new SemanticReleaseError(`Unsupported protocol \`${url.protocol}\``, 'EDOCKERODECFG', url);
			}

			/* c8 ignore next 3 */
			if (options.port === 2376) {
				options.protocol = 'https';
			}

			if (tlsVerify !== undefined) {
				options.protocol = tlsVerify ? 'https' : 'http';
			}
		}

		if (certPath !== undefined) {
			try {
				options.ca = splitca(path.join(certPath, 'ca.pem'));
				options.cert = await readFile(path.join(certPath, 'cert.pem'));
				options.key = await readFile(path.join(certPath, 'key.pem'));
			} catch (error) {
				throw new SemanticReleaseError(`Failed to read Docker TLS certificates from \`${certPath}\``, 'EDOCKERODECFG', error);
			}
		}

		if (timeout !== undefined) {
			try {
				options.timeout = Number.parseInt(timeout, 10);
			} catch {
				throw new SemanticReleaseError('Failed to parse Docker timeout', 'EDOCKERODECFG', timeout);
			}

			if (Number.isNaN(options.timeout)) {
				throw new SemanticReleaseError('Failed to parse Docker timeout as integer', 'EDOCKERODECFG', timeout);
			}
		}

		debug('Creating `dockerode` instance with %j', options);
		const docker = new Docker(options);
		logger.success('Created `dockerode` instance');
		return docker;
	})();

	// Validate the Dockerode instance
	try {
		const info = await docker.info();
		logger.success('`dockerode` successfully reported daemon information');
		debug('docker info: %j', info);
	} catch (error) {
		throw new SemanticReleaseError('`dockerode` instance could not report `info`', 'EDOCKERODE', error);
	}

	// Store persistent information in the `context` for later stages
	context.dockerode = {};

	// Get the authentication (if needed)
	context.dockerode.auth = await (async () => {
		const {
			username = env.DOCKERODE_USERNAME,
			password = env.DOCKERODE_PASSWORD,
			email = env.DOCKERODE_EMAIL ?? '',
			server = env.DOCKERODE_SERVER ?? docker.modem.host,
		} = pluginConfig;

		if ((username ?? password) === undefined) {
			return {};
		}

		if (username === undefined) {
			throw new SemanticReleaseError('Must provide `username` as well as `password`', 'EDOCKERODECFG');
		} else if (password === undefined) {
			throw new SemanticReleaseError('Must provide `password` as well as `username`', 'EDOCKERODECFG');
		}

		debug('Checking authentication');
		const authconfig = {username, password, email, serveraddress: server};
		await docker.checkAuth(authconfig)
			.catch(error => {
				const {password, ...rest} = authconfig;
				throw new SemanticReleaseError('Failed to get authentication token', 'EDOCKERODE', {error, password: password.replaceAll(/./g, '*'), ...rest});
			});

		logger.success('Authentication is valid');
		return {authconfig};
	})();

	// Resolve all the images
	context.dockerode.images = await Promise.all(
		images.map(async ({image, tag}) => {
			debug('Pulling image `%s`', image);
			const stream = await docker.pull(image, context.dockerode.auth).catch(error => {
				throw new SemanticReleaseError(`Failed to pull \`${image}\``, 'EDOCKERODE', error);
			});

			await follow(stream, docker.modem, `Failed to pull \`${image}\``);

			logger.success('Pulled image `%s`', image);
			return {image: docker.getImage(image), tag};
		}),
	);
}

export async function publish(pluginConfig, context) {
	await verifyConditions(pluginConfig, context);

	if (pluginConfig.images.length === 0) {
		return;
	}

	const {dockerode: {images, auth}, logger, env, nextRelease: {version}} = context;

	logger.log('Processing %i images', images.length);

	// Push all the images
	await Promise.all(
		images.map(async ({image, tag}) => {
			tag = template(tag)({...env, tag: parseRepositoryTag(image.name).tag, nextRelease: {version}});

			debug('Tagging image `%s` with new tag `%s`', image.name, tag);
			await image.tag({repo: image.name, tag}).catch(error => {
				throw new SemanticReleaseError(`Failed to tag \`${image.name}\``, 'EDOCKERODE', error);
			});

			debug('Pushing image `%s` with new tag `%s`', image.name, tag);
			const stream = await image.push({tag, ...auth}).catch(error => {
				throw new SemanticReleaseError(`Failed to push \`${image.name}\``, 'EDOCKERODE', error);
			});
			await follow(stream, image.modem, `Failed to push \`${image.name}\` for \`${tag}\``);

			logger.success('Pushed image `%s` with new tag `%s`', image.name, tag);
		}),
	);
}
